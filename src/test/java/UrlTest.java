import org.junit.Test;

import static org.junit.Assert.*;

public class UrlTest {

    @Test
    public void testProtocol() {
        Url testUrl = new Url("http://www.launchcode.com/test");
        assertEquals("http", testUrl.getProtocol());
    }

    @Test
    public void testHTTPSProtocol() {
        Url testUrl = new Url("https://www.launchcode.com/test");
        assertEquals("https", testUrl.getProtocol());
    }

    @Test
    public void testFTPProtocol() {
        Url testUrl = new Url("ftp://www.launchcode.com/test");
        assertEquals("ftp", testUrl.getProtocol());
    }

    @Test
    public void testFILEProtocol() {
        Url testUrl = new Url("file://www.launchcode.com/test");
        assertEquals("file", testUrl.getProtocol());
    }

    @Test
    public void testDomain() {
        Url testUrl = new Url("http://www.launchcode.com/test");
        assertEquals("launchcode.com", testUrl.getDomain());
    }

    @Test
    public void testDomainNoSubDomain() {
        Url testUrl = new Url("http://launchcode.com/test");
        assertEquals("launchcode.com", testUrl.getDomain());
    }

    @Test
    public void testPath() {
        Url testUrl = new Url("http://www.launchcode.com/test");
        assertEquals("test", testUrl.getPath());
    }

    @Test
    public void testMixedCapitalProtocol() {
        Url testUrl = new Url("HttPs://www.example.com/test");
        assertEquals("https", testUrl.getProtocol());
    }

    @Test
    public void testMixedCapitalDomain() {
        Url testUrl = new Url("HttPs://www.LAUNCHcode.com/test");
        assertEquals("launchcode.com", testUrl.getDomain());
    }

    @Test
    public void testMixedCapitalPath() {
        Url testUrl = new Url("HttPs://www.LAUNCHcode.com/TeST");
        assertEquals("test", testUrl.getPath());
    }

    @Test
    public void testUrlToString() {
        Url testUrl = new Url("http://www.launchcode.com/test");
        assertEquals("http://www.launchcode.com/test", testUrl.toString());
    }

    @Test
    public void testUrlToStringWithNoSubDomain() {
        Url testUrl = new Url("http://launchcode.com/test");
        assertEquals("http://launchcode.com/test", testUrl.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidProtocol() {
        Url testUrl = new Url("launch://www.launchcode.com/test");
        fail("the url protocol is not recognized");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyProtocol() {
        Url testUrl = new Url("//www.launchcode.com/test");
        fail("the url protocol is not recognized");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyDomain() {
        Url testUrl = new Url("http://");
        fail("the domain cannot be empty");
    }

    @Test
    public void testDomainWithEmptyPath() {
        Url testUrl = new Url("http://www.launchcode.com");
        assertEquals("launchcode.com", testUrl.getDomain());
    }

    @Test
    public void testEmptyPath() {
        Url testUrl = new Url("http://www.launchcode.com");
        assertEquals("", testUrl.getPath());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSpecialCharacterInDomain() {
        Url testUrl = new Url("http://www.launch@@#$$code.com");
        fail("Special Characters found");
    }
}
